## This is only a prototype ATM, so you should only use it as an example to implement your solution. or, feel free to send PRs :)

#!/bin/bash

set -ueo pipefail

NOW=$(date +%s)
TEMPERATURE="${1}"
HUMIDITY="${2}"
PRESSURE="${3}"

## these should be environment variables. It's cruical to setup correctly, but if any of those are not specified, the script will exit because the -u flag in the bash interpreter.
INFLUX_DB="${INFLUX_DB}"
INFLUX_ENDPOINT="${INFLUX_ENDPOINT}"
MOSQUITTO_ENDPOINT="${MOSQUITTO_ENDPOINT}"
MEASUREMENT="${MEASUREMENT}"

echo '### write.sh start ###'

echo "receive_timestamp;${MEASUREMENT};${NOW} temperature:${TEMPERATURE};humidity:${HUMIDITY};pressure:${PRESSURE}" 

echo "# ${0} >> writing to mqtt"
echo "receive_timestamp;${MEASUREMENT};${NOW} temperature:${TEMPERATURE};humidity:${HUMIDITY};pressure:${PRESSURE}" | timeout 1 mosquitto_pub -h ${MOSQUITTO_ENDPOINT} -t /${MEASUREMENT}/BME280/ -l

echo "# ${0} >> sending to influx"
curl -m 3 -i -XPOST "${INFLUX_ENDPOINT}/write?db=${INFLUX_DB}&precision=s" --data-binary "${MEASUREMENT} temperature=${TEMPERATURE},humidity=${HUMIDITY},pressure=${PRESSURE} ${NOW}"

echo "# ${0} >> ### write.sh end ###"


# BME280 Sensor (temperature, humidity, pressure)

## Connect rpi with the sensor

* Connect VCC to 3V (NOT 5V! it's just next to it, but will burn your sensor), GND to ground, SLC to SLC and SDA to SDA. It's written on the sensor, for raspberry pi, visit pinout.xyz/ - there is a GPIO pin which has a SQUARE instead of a circle (this is the 3V), you can use it as a reference. 

* power on RPI

* install ``` python-smbus ```

* download module with pip, or use what I'm using (its in the same directory as this readme): bme280.py


# Links
There are a lots of artictles on the internet about that, for example https://pypi.org/project/RPi.bme280/ 


# BME280

See BME280 directory to get information about this sensor (temperature, humidity, pressure)

# CCS811

This directory contains a submodule, the original version of the application

# CCS811\_updated

This directory contains an updated version of main.c from CCS811. See the README.md inside that directory for details

# RpiZeroW

## Initial setup

raspberry pi zero related stuff

1. Download Raspiban: https://www.raspberrypi.org/downloads/raspbian/
2. Mount the image locally:

```
losetup -P /dev/loop0 raspbian.img
mount /dev/loop0p2 /mnt
mount /dev/loop0p1 /mnt/boot
```

3. ``` touch /mnt/boot/ssh ``` 
	   "When the Pi boots, it looks for the ssh file. If it is found, SSH is enabled and the file is deleted. The content of the file does not matter; it could contain text, or nothing at all."
4. setup wpa_supplicant to make the Pi connect to wifi automatically; edit ``` /mnt/etc/wpa_supplicant/wpa_supplicant.conf ```
	   ```
		 ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
     update_config=1
     network={
       ssid="YOUR_SSID"
       psk="YOUR_PASSWORD"
     }
		 ```

5. Unmount the images

6. write the image, for example, with balena-etcher: https://www.balena.io/etcher/ 

7. wait 

8. insert SD card to RPI, boot, pray, wait for the default hostname (raspberrypi) to appear in the peer list in your router 

9. if succeed, wait a bit more, until ssh is started (the boot proccess should not take longer than a few minutes),
	    ``` ssh pi@<IP> ``` and enter the default password: ``` raspberry ``` 

10. If I have to remind you to change the default password, you did this whole thing for nothing,
	    and I strongly suggest to give up this project, and do something else, like fishing, hunting, or collecting stamps. 


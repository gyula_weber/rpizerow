#!/bin/bash

set -ueo pipefail

rm -f main.patched || true
patch ../CCS811/main.c ./main.patch -o main.patched

if diff main.c main.patched; then
	echo "# ${0} >> succeed"
else
	echo "# ${0} >> failed"
	exit 1
fi

exit 0


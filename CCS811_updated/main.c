// CCS811 Air Quality Sensor demo program
// Copyright (c) 2017 Larry Bank
// email: bitbank@pobox.com
// Project started 11/5/2017
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Updated by: Gyula Weber <gyula_weber@fastmail.com> @ 2019-12-15
// 	- sometimes, we stuck with <no data>, if it happens 3 times, 
// 	  we reset the chip and start again.
//  - We have an external writer which takes values as arguments,
//    and sends the data. It's commented out, so you should update
//    and un-comment, as needed.

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include <ccs811.h>

int main(int argc, char *argv[])
{
	int i,ccs811;
	int eCO2, TVOC;

	// Check which I2C channel you're using
	// Raspberry Pi's usually use channel 1
	// Other ARM boards tend to use channel 0
	ccs811 = ccs811Init(1, 0x5A);
	if (ccs811 != 0)
	{
		return -1; // problem - quit
	}
	printf("CCS811 device successfully opened.\n");
	printf("Allow 48 hours initial burn-in and 20 minutes to warm up (each use)\n");
	usleep(1000000); // wait for data to settle for first read
	char buffer[120];
	int data_not_ready = 0;
	for (i=0; ; i++) // read values once every 5 seconds for 30 minutes
	{
		if (ccs811ReadValues(&eCO2, &TVOC))
		{
			printf("eCO2 = %d, TVOC = %d\n", eCO2, TVOC);
			if (i < 5) { // wait a little before starting to send data
				printf("waiting a bit\n");
			} else {
				printf("not calling logger\n");
				// sprintf(buffer, "/home/pi/write_CCS811.sh %d %d", eCO2, TVOC);
				system(buffer);
			}
		}
		else {
			if (data_not_ready > 3) {
				printf("data not ready for the 3rd time, restarting");
				ccs811Shutdown();
				// wait a bit
				usleep(2000000);

				ccs811 = ccs811Init(1, 0x5A);
				if (ccs811 != 0)
				{
					printf("CCS811 initialization failed");
					return -1; // problem - quit
				}
			}
			data_not_ready++;
			printf("data not ready\n");
		}

		// ## no longer valid as the time has been modified ## printf("%02d:%02d min:sec have passed\n", (i*5)/60, (i*5) % 60);
		usleep(30000000); // every 30 seconds
	}

	return 0;
} /* main() */

